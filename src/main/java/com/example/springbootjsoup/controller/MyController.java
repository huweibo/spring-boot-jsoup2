package com.example.springbootjsoup.controller;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;
@RestController
@RequestMapping("/index")
public class MyController {
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/getdata",method = RequestMethod.GET)
    public Object say(@RequestParam("url") String url) throws IOException {
        Document document= Jsoup.connect(url).get();
        Elements element=document.select(".urldata li");
        String str = "";
        for (Element element1:element){
            System.out.println( element1 );
            str = str + element1;
        }
            return  str;
    }

}

