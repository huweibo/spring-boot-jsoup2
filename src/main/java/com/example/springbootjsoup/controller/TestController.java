package com.example.springbootjsoup.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller

public class TestController {
    @CrossOrigin(origins = "*")
    @GetMapping("/hello")
    @ResponseBody
    public String hello() {
        return "Hello World!";
    }

}
