package com.example.springbootjsoup.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;



/**
 * @ClassName: JsoupUtil
 * @Description:
 * @Author: zfc
 * @Date: 2021/8/23 11:28
 */
public class JsoupUtil {
    public static void main(String[] args) {
        JsoupUtil jsoupUtil=new JsoupUtil();
        jsoupUtil.jsoupList("http://111.6.107.78:18080/portal/hjxw/D2009index_1.htm");
    }

    public void jsoupList(String url){
        try {
            Document document= Jsoup.connect(url).get();
//            System.out.println("head="+document.head());
//            System.out.println("body="+document.body());
//            Element 和 Elements两种实现方法
//            使用标签获取数据
//            Elements element=document.select("a");
//            使用class类名获取数据
//            Elements element=document.getElementsByClass("c141512");
//            在div下面的li下面的a
//              Elements element=document.select("div > li > a");
//            c141512类名下的a标签
            Elements element=document.select(".pdnrlist li");
            for (Element element1:element){
                 System.out.println("<li><a>" +element1.attr("title")+element1.text()+"</a></li>");
                //System.out.println( element1 );
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("抛出异常");
        }
    }
}
