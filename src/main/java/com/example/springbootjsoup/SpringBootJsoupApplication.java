package com.example.springbootjsoup;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
public class SpringBootJsoupApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootJsoupApplication.class, args);
    }

}
